#!/bin/sh
#!/bin/bash
[ $# -ge 1 -a -f "$1" ] && input="$1" || input="-"
cat $input
msg="${input}    "

#tput civis # or tput vi maybe? hide cursor
while sleep 0.2
do
    printf -- "\r%s" "$msg"         # rewrite line
    msg="${msg#?}${msg%"${msg#?}"}" # 1st char to end
done
