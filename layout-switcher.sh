#!/bin/env sh
selection=$(echo -e "Deutsch\nHrvatski\nAmerican\nQuébécois" | rofi -dmenu -p "Keyboard Layouts")
case $selection in
    Deutsch) setxkbmap -layout de ;;
    Hrvatski) setxkbmap -layout hr ;;
    American) setxkbmap -layout us ;;
    Québécois) notify-send "Tabarnak" ;;
esac
