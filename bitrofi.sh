#!/bin/sh

operation=$(echo -e "get\nadd\ndel\nlock" | rofi -dmenu -p "Bitrofi" )
case "$operation" in
    get) rbw unlock && rbw get $(rbw list --fields name user folder | rofi -dmenu) | xclip -selection clipboard ;;
    add) length=$(rofi -dmenu -p "length") && user=$(rofi -dmenu -p "username") && name=$(rofi -dmenu -p "name") && rbw generate $length $name $user | xclip -selection clipboard;;
    del) rbw rm $(rbw list --fields name user | rofi -dmenu -p "which entry?");;
    lock) rbw lock;;
esac
rbw sync
